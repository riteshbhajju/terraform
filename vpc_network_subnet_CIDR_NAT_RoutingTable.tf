# we can create our own vpc to communicate with other vpc's etc
#vpc is region level in aws
#subnets are availability zone leve
#usuaaly create subnet in availibility zones as it is free
#while creating a subnet there is n way to determine if it is public or private
#subnets whic are associated with internet gateway are called public ubnets
#igw (internet gateway)  will be associated with subnetts using route tables
#subnets with internet gateway will allow the traffic from outsideto servers/any service
#webservers that have to be accessed from outside world is kept under public subnet
# private subnet : the idea is that they shpuld not be accessed from outside world... still they need internet for say download updates etc
# so we will crate NAT gateway and added to route table
# so public subnet we create igw and for public we create a NAT gateway
#the combination of the two sunbets makes a VPC
#NOT THESE IGW AND NAT WILL HAVE A ROUTE TABLES ATTACHES .these route tables will be mapped to each subnet in public/private subnets respectively
# we need to understand how many IPs we need before creating a VPC ...that is done by defining the CIDR
#Classless inter-domain routing (CIDR) is a set of Internet protocol (IP) standards that is used to create unique identifiers for networks and individual devices
#The formula to calculate the number of assignable IP address to CIDR networks is similar to classful networking. Subtract the number of network bits from 32. Raise 2 to that power and subtrct 2 for the network and broadcast addresses. For example, a /24 network has 232-24 - 2 addresses available for host assignment.
#https://erikberg.com/notes/networks.html - Complete networks
#here we are creating 2 subnets in the vpc .. Network range for vpc 10.0.0.0/16

#y default, Terraform stores state locally in a file named terraform.tfstate.
When working with Terraform in a team, use of a local file makes Terraform usage complicated because each user must make sure they always have the latest state data before running Terraform and make sure that nobody else runs Terraform at the same time.

#Wth remote state, Terraform writes the state data to a remote data store, which can then be shared between all members of a team. Terraform supports storing state in Terraform Cloud, HashiCorp Consul, Amazon S3, Alibaba Cloud OSS, and more.