variable "count_instance" {
 type = "number"
 default = 1
}

variable "instance_type" {
 type = "list"
 default = ["t2.micro,"t2.small,a1.large]                  #list is within square brackets []
}


resource "aws_instance" "web1" {
 count_instances   = length.(instance_type)
 ami               = "ami-004cd5eb616d96667"
 instance_type     = element(var.instance_type,count.index)    #everytime it will pick the elements of the instance type and create 
}
#count.index is required to keep the count of the the number of ements were picked so that all in the instance_type is picked
#now say we dont specify default of count_instances and give input 10... so 10 of each type of instance_type will be created.
#same is set but in list it is in order  whereas set can give any random order ... {} is used for set
#advance of the set is the memory set ... since does not need a order it does not need an index to store values hence memory freed
#learn objects also
intance_ip_addr
aws_instance.server.private_ip
depends_upon
