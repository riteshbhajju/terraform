variable "count_instance" {
 type = "number"
}

resource "aws_instance" "web" {
 count_instances   = var.count_instances
 ami               = "ami-004cd5eb616d96667"
 instance_type     = "t2.micro"
}


# count it a reserved variable so we have used a variable count_instance
# three types are there : string , boolean, number